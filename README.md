# Reversi 

Artificial intelligence for a modified version of Reversi 

### Branching model
Do whatever feature test and implementation you want in a **SEPARATE** branch than `master`.
 
When it's fully implemented and tested create a merge request to `master`.

Use a similar notation to the following for branch naming
+ `develop/{branch}` : general development of a feature or algorithm
+ `experiment/{branch}` : explore a certain feature further


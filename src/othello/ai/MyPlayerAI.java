package othello.ai;

import java.awt.Point;
import java.lang.Math;

import othello.model.Board;


public class MyPlayerAI extends ReversiAI {

    private static final int MAX_VAL = 10000;
    private static final int MIN_VAL = -10000;
    private String name;

    private static final boolean LOG_ENABLED = false;

    private final int OPENING_DEPTH = 8;
    private final int MID_DEPTH = 7;
    private final int END_DEPTH = 8;


    private int killerSlot;
    private Point[][] killerMoves;


    // stat variables
    private long cutoffCount = 0;
    private long branchExamedCount = 0;

    private int wPositional = 1, wEvaporation = 1, wEvaporation1 = -2,
            wEvaporation2 = 1, wMobility = 1, wfrontier = 1;

    // to counter time-limit
    private long nextMoveBeginTime = 0;
    private int timeLimit;

    private static int weighting[][] = {
            {0, 100, 10, 2, 2, 10, 100, 0},
            {100, -20, 1, 1, 1, 1, -20, 100},
            {10, 1, 1, 1, 1, 1, 1, 10},
            {2, 1, 1, 0, 0, 1, 1, 2},
            {2, 1, 1, 0, 0, 1, 1, 2},
            {10, 1, 1, 1, 1, 1, 1, 10},
            {100, -20, 1, 1, 1, 1, -20, 100},
            {0, 100, 10, 2, 2, 10, 100, 0}
    };


    public MyPlayerAI() {
        this.name = "9330643-9323573";

        this.killerSlot = 2;
        killerMoves = new Point[10][killerSlot];
        zeroKillerMoves();

        this.timeLimit = 1000 - 5; // ms

    }

    private void zeroKillerMoves() {
        for (int i = 0; i < killerMoves.length; i++) {
            for (int j = 0; j < killerMoves[i].length; j++) {
                killerMoves[i][j] = new Point(-9, -9);
            }
        }
    }

    private void playerLog(Object msg) {
        if (LOG_ENABLED)
            if (msg instanceof Board) {
                ((Board) msg).print();
            } else {
                System.out.println(msg);
            }
    }

    @Override
    public Point nextMove(Board b) {

        // start of timing current move
        nextMoveBeginTime = System.currentTimeMillis();

        zeroKillerMoves();


        return minimaxRoot(b, this.gameStatusDepth(b), true);
        /*{
            b.isCapturedByMe(x, y);					// square (x, y) is mine
			b.isCapturedByMyOpponennt(x, y);			// square (x, y) is for my opponent
			b.isEmptySquare(x, y);					// square (x, y) is empty
			b.move(x, y);							// attempt to place a piece at specified coordinates, and update
													// the board appropriately, or return false if not possible
			b.turn();								// end current player's turn
			b.print();								// ASCII printout of the current board
			if(b.getActive() == Board.WHITE)		//I am White
			if(b.getActive() == Board.BLACK)		//I am Black
			
			b.getMoveCount(true);					//number of possible moves for me
			b.getMoveCount(false);					//number of possible moves for my opponent
			b.getTotal(true);						//number of cells captured by me
			b.getTotal(false);						//number of cells captured by my opponent
			this.size;								//board size (always 8)
		}*/
    }

    private Point minimaxRoot(Board root, int depth, boolean maxPlayer) {

        Point bestMovePoint = null; // what if no move is possible?
        int bestScore = MIN_SCORE;
        Board b = new Board(root);

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (b.move(i, j)) {
                    branchExamedCount++;
                    int score = minimax(b, depth - 1, MIN_SCORE, MAX_SCORE, !maxPlayer);

                    if (score >= bestScore) {
                        bestScore = score;
                        bestMovePoint = new Point(i, j);
                    }
                    // undo last move by move()
                    b = new Board(root);
                }
            }
        }
        return bestMovePoint;
    }

    private boolean isKillerMove(int depth, int i, int j) {

        return (i == killerMoves[depth][0].x && j == killerMoves[depth][0].y) ||
                (i == killerMoves[depth][1].x && j == killerMoves[depth][1].y);
    }


    private int minimax(Board node, int depth, int alpha, int beta, boolean maxPlayer) {
        branchExamedCount++;

        if (System.currentTimeMillis() - nextMoveBeginTime >= timeLimit) {
            playerLog("preemptive return!");
            // MIN_SCORE so this branch will no be chosen, last fully searched branch
            // will be returned
            return MIN_SCORE;
        }
        node.turn();

        if (depth == 0) {
            return evaluation(node, maxPlayer);
        }

        return minimaxNonLeafNode(node, depth, alpha, beta, maxPlayer);

    }

    private Point[] treeMoves(int depth) {

        Point[] moves = new Point[64 + this.killerSlot];

        // fill the array with killer moves first
        for (int i = 0; i < this.killerSlot; i++) {

            moves[i] = new Point(killerMoves[depth][i].x,
                    killerMoves[depth][i].y);

        }

        // start where the last loop left off
        int index = this.killerSlot;

        // fill the rest with other moves
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (isKillerMove(depth, i, j)) {
                    // because killer moves is already in the list
                    // -9 is used as an invalid value
                    moves[index] = new Point(-9, -9);
                } else {
                    moves[index] = new Point(i, j);
                }
                index++;
            }
        }

        return moves;
    }

    private int minimaxNonLeafNode(Board node, int depth, int alpha, int beta, boolean maxPlayer) {
        /* same function for maxPlayer and minPlayer logic as they are very similar*/

        Board b = new Board(node);

        int bestScore = maxPlayer ? MIN_SCORE : MAX_SCORE;
        // looping children nodes

        Point[] moves = treeMoves(depth);


        for (int i = 0; i < moves.length; i++) {
            Point move = moves[i];

            /* it's already examined through killer moves */
            if (move.x < 0 || move.y < 0)
                continue;

            if (b.move(move.x, move.y)) {


                int score = minimax(b, depth - 1, alpha, beta, !maxPlayer);

                if (maxPlayer) {
                    bestScore = Math.max(bestScore, score);
                    alpha = Math.max(alpha, bestScore);

                } else {
                    bestScore = Math.min(bestScore, score);
                    beta = Math.min(beta, bestScore);

                }
                /* beta cutoff */
                if (beta <= alpha) {

                    // shift killer[0] to right and put current in place of it

                    killerMoves[depth][1] = killerMoves[depth][0];
                    killerMoves[depth][0] = new Point(move.x, move.y);

                    return bestScore;
                }

                b = new Board(node);
            }
        }
        return bestScore;

    }

    private int evaluation(Board board, boolean maxPlayer) {
        int positionalScore = 0;
        int mobilityScore = 0;
        int evaporationScore = 0;
        int frontierScore = 0;

        Board boardCp1 = new Board(board);
        if (!maxPlayer) {
            boardCp1.turn();
        }

        Board boardCp2 = new Board(boardCp1);

        int ourScore = board.getTotal(true);
        int opScore = board.getTotal(false);
        if (boardCp2.gameOver()) {
            if (ourScore > opScore) {
                return MAX_VAL;
            } else if (ourScore < opScore) {
                return MIN_VAL;
            } else
                return 0;
        }

        positionalScore = positionalEval(boardCp1, maxPlayer);
        evaporationScore = evaporationStrategy(boardCp1, maxPlayer);
        mobilityScore = mobilityStrategy(boardCp1, maxPlayer);
        frontierScore = frontierStrategy(boardCp1, maxPlayer);

        return positionalScore * wPositional
                + evaporationScore * wEvaporation
                + mobilityScore * wMobility
                + frontierScore * wfrontier;
    }

    private int positionalEval(Board board, boolean maxPlayer) {
        int score = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (board.isCapturedByMyOpponennt(i, j))
                    score -= weighting[i][j];
                else if (board.isCapturedByMe(i, j))
                    score += weighting[i][j];
            }
        }

        return score;
    }

    private int evaporationStrategy(Board board, boolean maxPlayer) {
        int numberOfMyStones = board.getTotal(true);
        int numberOfOpponentsStones = board.getTotal(false);
        int negation = numberOfMyStones - numberOfOpponentsStones;
        if (numberOfMyStones + numberOfOpponentsStones < 28) {
            return wEvaporation1 * negation;
        } else {
            return wEvaporation2 * negation;
        }
    }

    private int mobilityStrategy(Board board, boolean maxPlayer) {
        return board.getTotal(true) - board.getTotal(false);
    }

    private int frontierStrategy(Board board, boolean maxPlayer) {
        return board.getFrontierCount(false) - board.getFrontierCount(true);
    }

    private int gameStatusDepth(Board b) {
        if (b.getMoveCount(true) >= 10) {
            return MID_DEPTH;
        } else if (b.getMoveCount(true) > 7) {
            return OPENING_DEPTH;
        } else {
            return END_DEPTH;
        }
    }

    @Override
    public String getName() {
        return this.name;
    }
}
